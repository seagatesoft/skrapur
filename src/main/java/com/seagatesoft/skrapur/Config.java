package com.seagatesoft.skrapur;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {
	private static Config config = new Config();
	private Properties prop = new Properties();
	private Logger logger = LoggerFactory.getLogger(getClass());

	private Config() {
		try {
			prop.load(getClass().getResourceAsStream("/config.properties"));
		} catch (FileNotFoundException e) {
			logger.error("Fatal, can't find config.properties file. {}",
					e.getMessage());
		} catch (IOException e) {
			logger.error(
					"Fatal, error when loading config.properties file. {}",
					e.getMessage());
		}
	}

	public static Config getInstance() {
		return config;
	}
	
	public String getProperty(String propertyName) {
		return prop.getProperty(propertyName);
	}
}
