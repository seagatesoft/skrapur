package com.seagatesoft.skrapur;

import java.util.List;

import com.seagatesoft.skrapur.model.SearchRequest;
import com.seagatesoft.skrapur.model.SearchResult;

public interface ProviderNotifier {
	void notifySearchingDone(SearchRequest searchRequest,
			List<SearchResult> searchResults);

	void notifySearchingError(SearchRequest searchRequest, String errorMessage);
}
