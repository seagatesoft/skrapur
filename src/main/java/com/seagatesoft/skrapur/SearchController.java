package com.seagatesoft.skrapur;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.seagatesoft.skrapur.model.SearchRequest;
import com.seagatesoft.skrapur.model.Station;
import com.seagatesoft.skrapur.model.ValidationResult;

public class SearchController {
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("yyyyMMdd");
	private static final StationProvider STATION_PROVIDER = StationProvider.getInstance();

	private Config config = Config.getInstance();
	private final int maxBookingDays = Integer.parseInt(config.getProperty("max_booking_days"));
	private final int maxAdultPassengers = Integer.parseInt(config.getProperty("max_adult_passengers"));
	private final int maxChildPassengers = Integer.parseInt(config.getProperty("max_child_passengers"));
	private final int maxInfantPassengers = Integer.parseInt(config.getProperty("max_infant_passengers"));
	
	public int getMaxBookingDays() {
		return maxBookingDays;
	}
	
	public int getMaxAdultPassengers() {
		return maxAdultPassengers;
	}
	
	public int getMaxChildPassengers() {
		return maxChildPassengers;
	}
	
	public int getMaxInfantPassengers() {
		return maxInfantPassengers;
	}

	/*
	 * adult passengers: 0-4, child passengers: 0-4, infant passengers: 0-4
	 * departure date = today until today+90 origin and destination station must
	 * exists in the list
	 */
	public ValidationResult validateSearchRequest(SearchRequest searchRequest) {
		ValidationResult valResult = new ValidationResult();
		valResult.setValid(true);
		valResult.setMessage("Search request is valid.");

		DateTime today = DATE_FORMATTER.parseDateTime(DATE_FORMATTER.print(DateTime.now()));

		if (searchRequest.getAdultPassengers() < 0 ||
		    searchRequest.getChildPassengers() < 0 ||
		    searchRequest.getInfantPassengers() < 0) {
			valResult.setValid(false);
			valResult.setMessage("Number of passengers can't be negative.");
		} else if (searchRequest.getAdultPassengers() > maxAdultPassengers ||
		           searchRequest.getChildPassengers() > maxChildPassengers ||
		           searchRequest.getInfantPassengers() > maxInfantPassengers) {
			valResult.setValid(false);
			valResult.setMessage("Number of passengers can't be more than 4.");
		} else if (searchRequest.getDepartureDate().isBefore(today) ||
		           searchRequest.getDepartureDate().isAfter(today.plusDays(maxBookingDays+1))) {
			valResult.setValid(false);
			valResult.setMessage("Departure date must be between today and today+"+maxBookingDays);
		} else if (!STATION_PROVIDER.isStationCodeExists(searchRequest.getOrigin().getCode())) {
			valResult.setValid(false);
			valResult.setMessage("Origin station is not exists.");
		} else if (!STATION_PROVIDER.isStationCodeExists(searchRequest.getDestination().getCode())) {
			valResult.setValid(false);
			valResult.setMessage("Destination station is not exists.");
		}

		return valResult;
	}

	public void prepareSearchRequest(SearchRequest searchRequest) {
		Station origin = STATION_PROVIDER.getStation(searchRequest.getOrigin().getCode());
		Station destination = STATION_PROVIDER.getStation(searchRequest.getDestination().getCode());
		searchRequest.setOrigin(origin);
		searchRequest.setDestination(destination);
	}
}
