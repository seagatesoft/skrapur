package com.seagatesoft.skrapur;

import java.io.IOException;
import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.Response;
import com.seagatesoft.skrapur.helper.FileHelper;
import com.seagatesoft.skrapur.model.SearchRequest;
import com.seagatesoft.skrapur.model.SearchResult;

public class SearchResultResponseHandler extends AsyncCompletionHandler<List<SearchResult>> {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final SearchRequest searchRequest;
	private final SearchParser searchParser;
	private final ProviderNotifier providerNotifier;

	public SearchResultResponseHandler(
	        SearchRequest searchRequest,
	        SearchParser searchParser,
	        ProviderNotifier providerNotifier) {
		this.searchRequest = searchRequest;
		this.searchParser = searchParser;
		this.providerNotifier = providerNotifier;
	}

	@Override
	public List<SearchResult> onCompleted(Response response) {
		List<SearchResult> searchResults = null;

		try {
			
			if (logger.isTraceEnabled()) {
				StringBuilder fileName = new StringBuilder();
				DateTime departureDate = searchRequest.getDepartureDate();
				fileName
				    .append(searchRequest.getOrigin().getCode())
				    .append('-')
				    .append(searchRequest.getDestination().getCode())
				    .append('_')
				    .append(departureDate.getYear())
				    .append('-')
				    .append(departureDate.getMonthOfYear())
				    .append('-')
				    .append(departureDate.getDayOfMonth())
				    .append(".html");
				FileHelper.writeToFile(fileName.toString(), response.getResponseBody());
			}

			searchResults = searchParser.parse(response.getResponseBody());
			providerNotifier.notifySearchingDone(searchRequest, searchResults);
		} catch (IOException e) {
			providerNotifier.notifySearchingError(
			    searchRequest,
			    "Error when parsing search result: " + e.getMessage());
		}

		return searchResults;
	}

	@Override
	public void onThrowable(Throwable t) {
		providerNotifier.notifySearchingError(
		    searchRequest,
		    "Error when requesting search result: " + t.getMessage());
	}
}
