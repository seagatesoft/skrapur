package com.seagatesoft.skrapur;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.AsyncHttpClientConfig.Builder;
import com.ning.http.client.Cookie;
import com.ning.http.client.Request;
import com.ning.http.client.RequestBuilder;
import com.ning.http.client.Response;
import com.seagatesoft.skrapur.db.DatabaseConnector;
import com.seagatesoft.skrapur.helper.FileHelper;
import com.seagatesoft.skrapur.helper.IndonesianDateHelper;
import com.seagatesoft.skrapur.model.SearchRequest;
import com.seagatesoft.skrapur.model.SearchRequestJob;
import com.seagatesoft.skrapur.model.SearchResult;
import com.seagatesoft.skrapur.model.ValidationResult;

public class SearchProvider {
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("yyyyMMdd");

	public static final Map<String, String> CABIN_CLASSES = Maps.newHashMap();
	static {
		CABIN_CLASSES.put("K", "EKONOMI");
		CABIN_CLASSES.put("B", "BISNIS");
		CABIN_CLASSES.put("E", "EKSEKUTIF");
	}

	private final Config config = Config.getInstance();
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final Logger perfLogger = LoggerFactory.getLogger("performance");
	private final Logger consoleLogger = LoggerFactory.getLogger("console");
	private final AsyncHttpClient asyncHttpClient;
	private final SearchController controller = new SearchController();
	private final SearchParser searchParser = new SearchParser();
	private final DatabaseConnector dbConnector = DatabaseConnector.getInstance();
	private final String searchFormUrl = config.getProperty("search_form_url");
	private final int MAX_CONCURRENT_REQUESTS = Integer.parseInt(config
	    .getProperty("max_concurrent_requests"));

	private final AtomicInteger numberOfCurrentRequests = new AtomicInteger();
	private CountDownLatch searchAndPostLatch;
	private final Map<String, SearchRequestJob> jobsMap = Maps.newConcurrentMap();
	private final Map<String, Future<Future<List<SearchResult>>>> futuresMap = Maps
	    .newConcurrentMap();

	public SearchProvider() {
		Builder builder = new AsyncHttpClientConfig.Builder();
		builder.setRequestTimeoutInMs(Integer.parseInt(config.getProperty("http_timeout"))).build();
		asyncHttpClient = new AsyncHttpClient(builder.build());
	}

	public void closeAsyncHttpClient() {
		asyncHttpClient.close();
	}

	/**
	 * Synchronous search
	 * 
	 * @param searchRequest
	 * @return List of search result
	 * @throws Exception
	 */
	public List<SearchResult> search(SearchRequest searchRequest) throws Exception {
		ValidationResult valResult = controller.validateSearchRequest(searchRequest);

		if (!valResult.isValid()) {
			logger.error("Search request is not valid: {}", valResult.getMessage());
			throw new Exception("Search request is not valid: " + valResult.getMessage());
		}

		controller.prepareSearchRequest(searchRequest);

		Request searchFormRequest = new RequestBuilder()
		    .setMethod("GET")
		    .setUrl(searchFormUrl)
		    .build();
		long startTime = System.currentTimeMillis();
		Response searchFormResponse = asyncHttpClient
		    .prepareRequest(searchFormRequest)
		    .execute()
		    .get();
		Document searchForm = Jsoup.parse(searchFormResponse.getResponseBody());
		String formAction = searchForm.select("form#input").first().attr("action");
		long duration = System.currentTimeMillis() - startTime;

		if (logger.isInfoEnabled()) {
			logger.info("Get form action time = {} ms", duration);
		}

		DateTime departureDate = searchRequest.getDepartureDate();
		int monthIndex = departureDate.getMonthOfYear();
		int dayIndex = departureDate.getDayOfWeek();
		String departureDateString = new StringBuilder()
		    .append(IndonesianDateHelper.getDayName(dayIndex))
		    .append(", ")
		    .append(departureDate.getDayOfMonth())
		    .append(" ")
		    .append(IndonesianDateHelper.getMonthName(monthIndex))
		    .append(" ")
		    .append(departureDate.getYear())
		    .toString();

		RequestBuilder ticketSearchRequestBuilder = new RequestBuilder()
		    .setMethod("POST")
		    .setUrl(searchFormUrl + formAction)
		    .addHeader("Referer", searchFormUrl)
		    .addParameter("Submit", "Tampilkan")
		    .addParameter("origination", searchRequest.getOrigin().toString())
		    .addParameter("destination", searchRequest.getDestination().toString())
		    .addParameter(
		        "tanggal",
		        DATE_FORMATTER.print(departureDate) + "#" + departureDateString)
		    .addParameter("adult", searchRequest.getAdultPassengersString())
		    .addParameter("children", searchRequest.getChildPassengersString())
		    .addParameter("infant", searchRequest.getInfantPassengersString());

		for (Cookie cookie : searchFormResponse.getCookies()) {
			ticketSearchRequestBuilder.addCookie(cookie);
		}

		Request ticketSearchRequest = ticketSearchRequestBuilder.build();
		startTime = System.currentTimeMillis();
		Response ticketSearchResponse = asyncHttpClient
		    .prepareRequest(ticketSearchRequest)
		    .execute()
		    .get();
		duration = System.currentTimeMillis() - startTime;

		if (logger.isInfoEnabled()) {
			logger.info("Response time = {} ms", duration);
		}

		if (logger.isTraceEnabled()) {
			FileHelper.writeToFile("result.html", ticketSearchResponse.getResponseBody());
		}

		startTime = System.currentTimeMillis();
		List<SearchResult> searchResults = searchParser.parse(ticketSearchResponse
		    .getResponseBody());
		duration = System.currentTimeMillis() - startTime;

		if (logger.isInfoEnabled()) {
			logger.info("Parsing time = {} ms", duration);
		}

		return searchResults;
	}

	/**
	 * Asynchronous search
	 * 
	 * @param searchRequest
	 * @return
	 * @throws Exception
	 */
	public Future<Future<List<SearchResult>>> searchAndPost(SearchRequest searchRequest)
	        throws Exception {
		return searchAndPost(searchRequest, new ProviderNotifier() {

			public void notifySearchingDone(SearchRequest searchRequest,
			        List<SearchResult> searchResults) {
			}

			public void notifySearchingError(SearchRequest searchRequest, String errorMessage) {

			}
		});
	}

	/**
	 * Asynchronous search
	 * 
	 * @param searchRequest
	 * @return
	 * @throws Exception
	 */
	public Future<Future<List<SearchResult>>> searchAndPost(SearchRequest searchRequest,
	        ProviderNotifier providerNotifier) throws Exception {
		ValidationResult valResult = controller.validateSearchRequest(searchRequest);

		if (!valResult.isValid()) {
			throw new Exception("Search request is not valid: " + valResult.getMessage());
		}

		controller.prepareSearchRequest(searchRequest);
		Request searchFormRequest = new RequestBuilder()
		    .setMethod("GET")
		    .setUrl(searchFormUrl)
		    .build();
		Future<Future<List<SearchResult>>> future = asyncHttpClient.prepareRequest(
		    searchFormRequest).execute(
		    new SearchFormResponseHandler(
		                                  asyncHttpClient,
		                                  searchRequest,
		                                  searchParser,
		                                  providerNotifier));

		return future;
	}

	public void bulkSearchAndPost(List<SearchRequest> searchRequests) {
		int numberOfSearchRequests = searchRequests.size();
		int searchId = 0;
		long startTime = System.currentTimeMillis();
		ProviderNotifierImpl providerNotifier = new ProviderNotifierImpl();
		searchAndPostLatch = new CountDownLatch(numberOfSearchRequests);

		if (logger.isInfoEnabled()) {
			logger.info("Start bulk search for {} search requests.", numberOfSearchRequests);
		}

		long trackOne = System.currentTimeMillis();
		while (!searchRequests.isEmpty()) {
			if (!isMaxRequestsReached()) {
				SearchRequest searchRequest = searchRequests.remove(0);
				searchRequest.setId(Integer.toString(searchId));
				searchId++;

				SearchRequestJob job = new SearchRequestJob(searchRequest);
				job.setSearchStartTime(DateTime.now());
				jobsMap.put(searchRequest.getId(), job);

				if (logger.isInfoEnabled()) {
					logger.info("Start searching for request = {}", searchRequest);
				}

				try {
					addNumberOfCurrentRequests();
					Future<Future<List<SearchResult>>> future = searchAndPost(
					    searchRequest,
					    providerNotifier);
					futuresMap.put(searchRequest.getId(), future);
				} catch (Exception e) {
					providerNotifier.notifySearchingError(
					    searchRequest,
					    "Error when start searching for request: " + e.getMessage());
				}
			} else if (System.currentTimeMillis() - trackOne >= 60000) {
				if (consoleLogger.isInfoEnabled()) {
					consoleLogger.info("Number of current requests: {}", getNumberOfCurrentRequests());
					consoleLogger.info("Number of unprocessed search requests: {}", searchRequests.size());

					for (String key : futuresMap.keySet()) {
						consoleLogger.info("SearchRequest: {}", jobsMap.get(key));
					}
				} else if (logger.isInfoEnabled()) {
					logger.info("Number of current requests: {}", getNumberOfCurrentRequests());
					logger.info("Number of unprocessed search requests: {}", searchRequests.size());

					for (String key : futuresMap.keySet()) {
						logger.info("SearchRequest: {}", jobsMap.get(key));
					}
				}

				trackOne = System.currentTimeMillis();
			}
		}

		try {
			searchAndPostLatch.await(
			    Long.parseLong(config.getProperty("max_bulk_requests_timeout")),
			    TimeUnit.MILLISECONDS);
		} catch (NumberFormatException e) {
			logger.error(
			    "Invalid value for max_bulk_requets_timeout in config.properties: {} minutes.",
			    config.getProperty("max_bulk_requests_timeout"));
		} catch (InterruptedException e) {
			logger.error(
			    "InterruptedException when waiting for bulk search requests: {}",
			    e.getMessage());
		}

		if (logger.isInfoEnabled()) {
			logger.info("Start redo searching for failed or zero result searches.");
		}

		for (SearchRequestJob job : jobsMap.values()) {
			if (job.getNumberOfResults() == 0) {
				SearchRequest searchRequest = job.getSearchRequest();

				if (logger.isInfoEnabled()) {
					logger.info("Redo search for {}", searchRequest);
				}

				try {
					List<SearchResult> searchResults = search(searchRequest);
					dbConnector.saveSearchResults(searchRequest, searchResults);

					if (logger.isInfoEnabled()) {
						logger.info(
							"Finished redo search for {}. Found {} results.", 
							searchRequest, 
							searchResults.size());
					}
				} catch (Exception e) {
					logger.error(
						"Error when searching and posting for {}. Message: {}",
						searchRequest,
						e.getMessage());
				}
			}
		}

		long duration = System.currentTimeMillis() - startTime;

		if (logger.isInfoEnabled()) {
			logger.info(
			    "Finished bulk search for {} search requests in {} ms.",
			    numberOfSearchRequests,
			    duration);

			perfLogger
			    .info("ID,ORIGIN,DESTINATION,DEPARTURE DATE,ADULT,CHILD,INFANT,SEARCH START,SEARCH END,SEARCH TIME,POST TIME,RESULTS,ERROR,MESSAGE");

			for (Map.Entry<String, SearchRequestJob> entry : jobsMap.entrySet()) {
				perfLogger.info("{}", entry.getValue());
			}
		}
	}

	private int addNumberOfCurrentRequests() {
		return numberOfCurrentRequests.incrementAndGet();
	}

	private int subtractNumberOfCurrentRequests() {
		return numberOfCurrentRequests.decrementAndGet();
	}

	private int getNumberOfCurrentRequests() {
		return numberOfCurrentRequests.get();
	}

	private boolean isMaxRequestsReached() {
		return getNumberOfCurrentRequests() >= MAX_CONCURRENT_REQUESTS;
	}

	private class ProviderNotifierImpl implements ProviderNotifier {

		public void notifySearchingDone(SearchRequest searchRequest,
		        List<SearchResult> searchResults) {
			SearchRequestJob job = jobsMap.get(searchRequest.getId());
			job.setSearchEndTime(DateTime.now());
			job.setNumberOfResults(searchResults.size());
			job.setError(false);
			futuresMap.remove(searchRequest.getId());

			dbConnector.saveSearchResults(searchRequest, searchResults);
			job.setPostEndTime(DateTime.now());

			subtractNumberOfCurrentRequests();

			if (logger.isInfoEnabled()) {
				logger.info(
				    "Finished searching for request: {}. Found {} result(s). Latch = {}",
				    searchRequest,
				    searchResults.size(),
				    searchAndPostLatch.getCount());
			}

			searchAndPostLatch.countDown();
		}

		public void notifySearchingError(SearchRequest searchRequest, String errorMessage) {
			SearchRequestJob job = jobsMap.get(searchRequest.getId());
			job.setSearchEndTime(DateTime.now());
			job.setError(true);
			job.setErrorMessage(errorMessage);
			futuresMap.remove(searchRequest.getId());

			subtractNumberOfCurrentRequests();

			logger.error(
			    "Error when searching for {}. Message : {}. Latch = {}",
			    searchRequest,
			    errorMessage,
			    searchAndPostLatch.getCount());

			searchAndPostLatch.countDown();
		}
	}
}
