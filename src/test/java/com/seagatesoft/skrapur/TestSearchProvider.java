package com.seagatesoft.skrapur;

import java.util.List;
import java.util.concurrent.Future;

import junit.framework.TestCase;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.seagatesoft.skrapur.model.SearchRequest;
import com.seagatesoft.skrapur.model.SearchResult;
import com.seagatesoft.skrapur.model.Station;

public class TestSearchProvider extends TestCase {
	private final Logger consoleLogger = LoggerFactory.getLogger("console");
	private final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("yyyyMMdd");

	public void testSearch() {
		SearchRequest searchRequest = createSearchRequest();
		SearchProvider provider = new SearchProvider();

		try {
			List<SearchResult> searchResults = provider.search(searchRequest);
			assertFalse(searchResults.isEmpty());

			if (consoleLogger.isInfoEnabled()) {
				consoleLogger.info("Found {} results.", searchResults.size());

				for (SearchResult searchResult : searchResults) {
					consoleLogger.info(
					    "Cabin class = {}",
					    SearchProvider.CABIN_CLASSES.get(searchResult.getCabinClass()));
					consoleLogger.info("Search result = {}", searchResult);
				}
			}
		} catch (Exception e) {
			consoleLogger.error("Error when scraping: {}", e.getMessage());
			e.printStackTrace();
		}
	}

	public void testSearchAndPost() {
		SearchRequest searchRequest = createSearchRequest();
		SearchProvider provider = new SearchProvider();

		try {
			Future<Future<List<SearchResult>>> future = provider.searchAndPost(searchRequest);

			while (!future.isDone()) {
			}

			Future<List<SearchResult>> deeperFuture = future.get();

			while (!deeperFuture.isDone()) {
			}
			
			List<SearchResult> searchResults = deeperFuture.get();
			
			consoleLogger.info("Finished searching, found {} result(s)", searchResults.size());
			
			if (consoleLogger.isDebugEnabled()) {
				for (SearchResult searchResult : searchResults) {
					consoleLogger.debug(
					    "Cabin class = {}",
					    SearchProvider.CABIN_CLASSES.get(searchResult.getCabinClass()));
					consoleLogger.trace("Search result = {}", searchResult);
				}
			}
		} catch (Exception e) {
			consoleLogger.error("Error when search and post: {}", e.getMessage());
			e.printStackTrace();
		}
	}

	private SearchRequest createSearchRequest() {
		SearchRequest searchRequest = new SearchRequest();
		Station origin = new Station();
		origin.setCode("YK");
		searchRequest.setOrigin(origin);
		Station destination = new Station();
		destination.setCode("GMR");
		searchRequest.setDestination(destination);
		searchRequest.setDepartureDate(DATE_FORMATTER.parseDateTime("20131027"));
		searchRequest.setAdultPassengers(1);
		searchRequest.setChildPassengers(0);
		searchRequest.setInfantPassengers(0);

		return searchRequest;
	}
}
