package com.seagatesoft.skrapur;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import junit.framework.TestCase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.seagatesoft.skrapur.model.Station;

public class TestStationProvider extends TestCase {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public void testScrapeStationList() throws InterruptedException, ExecutionException,
	        IOException {
		StationProvider provider = StationProvider.getInstance();
		List<Station> stations = provider.scrapeStationList();
		assertFalse("Should get station list!", stations.isEmpty());

		if (logger.isInfoEnabled()) {
			logger.info("Number of stations = {}", stations.size());
			
			for (Station station : stations) {
				logger.debug("Station = {}, city = {}", station, station.getCity());
			}
		}
	}

	public void testWriteStationListToFile() throws InterruptedException, ExecutionException,
	        IOException {
		StationProvider provider = StationProvider.getInstance();
		provider.writeStationListToFile("src/main/resources/stations.csv");
	}
}
