package com.seagatesoft.skrapur;

import org.joda.time.DateTime;

import com.seagatesoft.skrapur.model.SearchRequest;
import com.seagatesoft.skrapur.model.Station;

import junit.framework.TestCase;

public class TestSearchController extends TestCase {
	private SearchController searchController = new SearchController();

	public void testValidateSearchRequest() {
		SearchRequest searchRequest = new SearchRequest();
		Station origin = new Station();
		origin.setCode("PSE");
		Station destination = new Station();
		destination.setCode("YK");
		searchRequest.setOrigin(origin);
		searchRequest.setDestination(destination);
		searchRequest.setDepartureDate(DateTime.now());
		searchRequest.setAdultPassengers(1);
		searchRequest.setChildPassengers(1);
		searchRequest.setInfantPassengers(0);
		assertTrue(searchController.validateSearchRequest(searchRequest).isValid());
		
		origin.setCode("");
		assertFalse(searchController.validateSearchRequest(searchRequest).isValid());
		
		origin.setCode("PSE");
		destination.setCode(null);
		assertFalse(searchController.validateSearchRequest(searchRequest).isValid());
		
		destination.setCode("YK");
		searchRequest.setDepartureDate(DateTime.now().minusDays(1));
		assertFalse(searchController.validateSearchRequest(searchRequest).isValid());
		
		searchRequest.setDepartureDate(DateTime.now().plusDays(90));
		assertFalse(searchController.validateSearchRequest(searchRequest).isValid());
		
		searchRequest.setDepartureDate(DateTime.now().plusDays(30));
		searchRequest.setAdultPassengers(-1);
		assertFalse(searchController.validateSearchRequest(searchRequest).isValid());
		
		searchRequest.setAdultPassengers(5);
		assertFalse(searchController.validateSearchRequest(searchRequest).isValid());
		
		searchRequest.setAdultPassengers(4);
		searchRequest.setChildPassengers(5);
		assertFalse(searchController.validateSearchRequest(searchRequest).isValid());
		
		searchRequest.setChildPassengers(4);
		searchRequest.setInfantPassengers(5);
		assertFalse(searchController.validateSearchRequest(searchRequest).isValid());
	}
	
	public void testPrepareSearchRequest() {
		SearchRequest searchRequest = new SearchRequest();
		Station origin = new Station();
		origin.setCode("PSE");
		Station destination = new Station();
		destination.setCode("YK");
		searchRequest.setOrigin(origin);
		searchRequest.setDestination(destination);
		
		searchController.prepareSearchRequest(searchRequest);
		
		assertEquals("PASAR SENEN", searchRequest.getOrigin().getName());
		assertEquals("JAKARTA", searchRequest.getOrigin().getCity());
		assertEquals("YOGYAKARTA", searchRequest.getDestination().getName());
		assertEquals("YOGYAKARTA", searchRequest.getDestination().getCity());
	}
}
