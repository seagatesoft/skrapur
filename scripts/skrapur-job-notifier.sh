#!/bin/bash
errors=$(grep ERROR skrapur.log)
finish_line=$(grep "Finished searching in" skrapur.log | awk '/./{line=$0} END{print line}')

SUBJECT="Skrapur Job Notifier"
EMAIL="sigitdewanto11@yahoo.co.uk"
MESSAGE=$errors$finish_line
MAIL_FILE="/tmp/skrapur-mail.txt"

echo $MESSAGE
echo $MESSAGE > $MAIL_FILE

/usr//bin/mail -s "$SUBJECT" "$EMAIL" -- -f "sigit@seagatesoft.com" < $MAIL_FILE

